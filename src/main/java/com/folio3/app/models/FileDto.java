package com.folio3.app.models;

import java.io.Serializable;

public class FileDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fileName;

	private String content;

	public FileDto() {

	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
