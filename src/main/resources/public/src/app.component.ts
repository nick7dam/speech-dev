import {Component, Vue, Watch} from 'vue-property-decorator'
import RecorderComponent from "@/components/recorder/recorder.vue";
import ProjectExplorerComponent from "@/components/projectExplorer/projectExplorer.vue";
import {Actions} from "@/shared/helpers";

@Component({
  components: {
    RecorderComponent,
    ProjectExplorerComponent
  }
})
export default class App extends Vue {
  public directory:string
  public explorerConfig:any
  public actions:any
  constructor() {
    super();
    this.directory = ''
    this.actions = Actions
    this.explorerConfig = {
      showHiddenDir: false,
      absolutePath: '/Users/nick/projects/'
    }
  }
  mounted() {
  }

  created() {
  }

  public updateProjectExplorer(e:any) {
    this.directory = e
  }
}
