import {Component, Vue, Watch} from "vue-property-decorator";
import {NewComponentQuestions} from "@/shared/helpers";

@Component({
  props: {
    questionType: String,
    questionIndex: Number
  }
})
export default class QuestionComponent extends Vue {
  public nextQuestion: string
  public answer: string
  public allQuestions: string[]
  public allAnswers: string[]

  constructor() {
    super();
    this.nextQuestion = ''
    this.answer = ''
    this.allQuestions = []
    this.allAnswers = []
  }
  @Watch('questionType', {immediate: true, deep: true})
  public updateQuestion(newVal:string){
    if(newVal){
      switch (newVal) {
        case 'component':
          this.allQuestions = NewComponentQuestions
      }
    }
  }
  @Watch('questionIndex', {immediate: true, deep: true})
  public updateQuestionIndex(newVal:number){
    if(newVal > -1){
      if(newVal < this.allQuestions.length){
        this.nextQuestion = this.allQuestions[newVal]
      }
      else
        this.$emit('questionsCompleted', {question: this.allQuestions, answers: this.allAnswers})
    }
  }
  public inputCommand(){
    if(this.answer) {
      this.$emit('onCommand', {command: this.answer, question: this.nextQuestion})
      this.allAnswers.push(this.answer)
      this.nextQuestion = ''
      this.answer = ''
    } else {
      alert('Please type in an answer')
    }
  }
}
