import {Component, Vue} from "vue-property-decorator";
import * as Recorder from '@/shared/recorder.js'
import WatsonService from "@/shared/services/watsonService";
import {AxiosResponse} from "axios";
import QuestionComponent from "@/components/questionComponent/question.vue";
import {Actions} from "@/shared/helpers";

@Component({
  props:{
    disableRecording:Boolean
  },
  components: {
    QuestionComponent,
  }
})
export default class RecorderComponent extends Vue {
  public isRecording: boolean
  public transcriptLoading: boolean
  public gumStream: any
  public input: any
  public transcript: string[]
  public nextUserQuestion: number
  public questionType: string
  public rec: any
  public actions: any
  public recorder: any
  public audioContext: AudioContext | null
  public watsonService: any

  constructor() {
    super();
    this.isRecording = false
    this.transcriptLoading = false
    this.gumStream = null
    this.input = null
    this.transcript = []
    this.rec = null
    this.actions = Actions
    this.nextUserQuestion = -1
    this.questionType = ''
    this.recorder = Recorder
    this.audioContext = null
    this.watsonService = WatsonService.getInstance()
  }

  public askWatson(dto: FormData) {
    let self = this
    return new Promise(resolve => {
      self.watsonService.post(dto).then((resp: AxiosResponse) => {
        resolve(resp.data)
      })
    })
  }

  public readFile(blob: any) {
    let self = this
    let file = new FileReader();
    file.onload = function (e) {
      let base64Audio: any = this.result;
      let data: FormData = new FormData();
      if (base64Audio)
        data.append('audio', base64Audio.split(',')[1]);
      self.askWatson(data).then((result: string) => {
        self.transcript.push(`${result} (user voice)`)
        self.transcriptLoading = false
        self.detectAction(result)
      })
    }
    file.readAsDataURL(blob);
  }

  public detectAction(action: string) {
    let index = this.actions.findIndex((e: any) => e.value.includes(action.trim()))
    if (index > -1) {
      this.questionType = this.actions[index].action
      this.nextUserQuestion = 0
    }
  }

  public stopRecording() {
    if (this.rec && this.rec.stop)
      this.rec.stop()
    this.gumStream.getAudioTracks()[0].stop();
    console.log("Recording Stopped");
    this.isRecording = false
    this.rec.exportWAV(this.readFile)
    this.transcriptLoading = true
  }

  public startRecording(e: any) {
    e.preventDefault()
    let self = this
    setTimeout(function () {
      self.stopRecording()
    }, 1500)
    let constraints = {audio: true, video: false}
    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
      console.log("getUserMedia");
      //@ts-ignore
      self.audioContext = new (window.AudioContext || window.webkitAudioContext)();
      self.gumStream = stream;
      self.input = self.audioContext.createMediaStreamSource(stream);
      //@ts-ignore
      self.rec = new self.recorder(self.input, {
        numChannels: 1
      });
      self.rec.record()
      console.log("Recording started");
      self.isRecording = true
    }).catch(function (err) {
      self.isRecording = false
    });
  }

  public uploadFiles(e: any) {
    this.$emit('updateProjectExplorer', e)
  }

  public onCommand(command: any) {
    this.transcript.push(`${command.question} (question)`)
    this.transcript.push(`${command.command} (user input)`)
    this.nextUserQuestion++
  }
  public questionsCompleted(e:any) {
    console.log(e)
    debugger
  }
}
