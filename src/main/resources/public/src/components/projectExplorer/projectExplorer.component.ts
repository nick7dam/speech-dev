import {Component, Vue, Watch} from "vue-property-decorator";
import StructureService from "@/shared/services/structureService";
import {AllMainFilesList} from "@/shared/helpers"

@Component({
  props: {
    directory: {
      type: [String, Event],
      required: true
    },
    config: {
      type: Object,
      required: true
    }
  }
})
export default class ProjectExplorerComponent extends Vue {
  public projectStructure: any
  public structureService: any
  public projectMainFiles: any
  public projectFiles: any[]
  public projectType: string
  public allMainFilesList: any[]

  constructor() {
    super();
    this.projectStructure = {}
    this.projectMainFiles = []
    this.projectType = 'vuejs'
    this.projectFiles = []
    this.allMainFilesList = AllMainFilesList
    this.structureService = StructureService.getInstance()
  }

  @Watch('directory', {immediate: true, deep: true})
  public updateProjectStructure(newVal: any) {
    let self = this
    if (newVal) {
      const files = newVal.currentTarget.files
      let dirs = []
      for (let i = 0; i < files.length; i++) {
        let e = files[i]
        let path = e.webkitRelativePath
        let splitPath = path.split('/')
        dirs.push({
          file: e,
          fileName: splitPath[splitPath.length - 1],
          path: splitPath.splice(0, splitPath.length - 1).join('/')
        })
      }
      this.projectFiles = dirs
    }
  }

  @Watch('config', {immediate: true, deep: true})
  public updateConfig(newVal: any) {
    if (newVal) {

    }
  }

  @Watch('projectFiles', {immediate: true, deep: true})
  public updateProjectFiles(newVal: any) {
    if (newVal && newVal.length) {
      this.getNeededProjectFiles(newVal)
    }
  }

  public getNeededProjectFiles(files: any[]) {
    let self = this
    if (this.projectType === 'vuejs') {
      files.forEach((e: any) => {
        let index = self.allMainFilesList.findIndex((listItem: any) => e.path.includes(listItem) && e.fileName.charAt(0) !== '.')
        if (index > -1)
          self.projectMainFiles.push({
            ...e,
            fileType: self.allMainFilesList[index]
          })
      })
      //this.projectMainFiles = new Set(this.projectMainFiles)
      console.log(this.projectMainFiles)
    } else if (this.projectType === 'reactjs') {
      //TODO generate files for react project
    }
  }
}
