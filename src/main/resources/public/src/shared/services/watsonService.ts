import BaseEntityService from "@/shared/services/baseEntityService";

export default class WatsonService extends BaseEntityService<any> {
  private static instance: WatsonService;

  private constructor () {
    super('/speech/transcribe')
  }

  public static getInstance (): WatsonService {
    if (!WatsonService.instance) {
      return new WatsonService()
    }
    return WatsonService.instance
  }
}
