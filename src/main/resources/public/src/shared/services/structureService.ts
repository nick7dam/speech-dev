import BaseEntityService from "./baseEntityService";

export default class StructureService extends BaseEntityService<any> {
  private static instance: StructureService;

  private constructor () {
    super('/structure')
  }

  public static getInstance (): StructureService {
    if (!StructureService.instance) {
      return new StructureService()
    }
    return StructureService.instance
  }
}
